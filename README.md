# FORINCOM PLATFORM GATEWAY FOR GENESBLUE v1

## Spec documentation

- [API documentation](https://gitlab.com/forincom-cl-docs/platform-gateway-genesblue/-/blob/master/openapi.yaml).

## Caveats

- The API only can be accessed by a API key. You should have one or contact to [María Jesús Guzmán](mailto:mguzman@forincom.cl), our CEO, to give you one.
- The API implements a rate limit mechanism, so you will be restricted of doing to many requests in a short range of time.

## Troubleshoots

- The try out feature of the GitLab swagger viewer doesn't work. Instead you could use the [official swagger editor](https://editor.swagger.io/) and try out the API from there either by copying the content of the `openapi.yaml` and pasting it there or by using the `import URL` options at the file menu of the official swagger editor and pasting this URL: `https://glcdn.githack.com/forincom-cl-docs/platform-gateway-genesblue/-/raw/master/openapi.yaml`.

---
© 2020